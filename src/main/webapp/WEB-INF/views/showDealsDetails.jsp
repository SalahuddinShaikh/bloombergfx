<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Deals Details</title>
<link href="<c:url value='/static/css/bootstrap.css' />"
	rel="stylesheet" type="text/css"></link>
<link href="<c:url value='/static/css/app.css' />" rel="stylesheet"
	type="text/css"></link>
</head>
<body>
	<form method="GET" action="deals" class="form-inline">
		<div class="row">
			<div class="form-group col-md-12">
				<div class="col-md-7" style="padding: 20px">
					<input type="submit" value="Get count of deals per currency"
						class="btn btn-primary btn-sm" />
				</div>
			</div>

		</div>
		<div class="table-responsive">
			<c:if test="${not empty deals}">
				<table>
					<table class="table">
						<thead>
							<tr>
								<th>#</th>
								<th>Currency Code</th>
								<th>Count</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${deals}" var="d" varStatus="loop">
								<tr>
									<td>${loop.index+1}</td>
									<td>${d.crncyCd}</td>
									<td>${d.dealCount}</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
			</c:if>
		</div>
	</form>
</body>
</html>
