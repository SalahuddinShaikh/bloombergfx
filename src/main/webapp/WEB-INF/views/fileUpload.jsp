<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Spring 4 MVC File Upload Example</title>
<link href="<c:url value='/static/css/bootstrap.css' />"
	rel="stylesheet" type="text/css"></link>
<link href="<c:url value='/static/css/app.css' />" rel="stylesheet"
	type="text/css"></link>
</head>
<body>

	<form method="POST" action="upload" enctype="multipart/form-data"
		class="form-inline">
		<div class="row">
			<div class="form-group col-md-12">
				<div class="col-md-7" style="padding: 20px">
					<input type="file" id="file" name="file"
						class="form-control input-sm" /> <input type="submit"
						value="Upload" class="btn btn-primary btn-sm" />
				</div>
			</div>
		</div>
	</form>

</body>
</html>
