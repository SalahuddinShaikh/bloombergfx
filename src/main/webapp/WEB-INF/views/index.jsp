<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
    <html>

    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet prefetch" href="static/css/bootstrap.min.css">
        <style class="cp-pen-styles">
            body {
                padding: 10px;
            }
            
            #exTab1 .tab-content {
                color: white;
                background-color: #428bca;
                padding: 5px 15px;
            }
            /* remove border radius for the tab */
            
            #exTab1 .nav-pills > li > a {
                border-radius: 0;
            }
        </style>

    </head>

    <body>

        <div class="container">
            <h1>Bloomberg FX Deals Ingester</h1></div>
        <div id="exTab1" class="container">
            <ul class="nav nav-pills">
                <c:if test="${(empty t1) and (empty t2)}">
                    <c:set var="t1" value="active" />
                </c:if>
                <li class="${t1}">
                    <a href="#2a" data-toggle="tab">File Upload</a>
                </li>
                <li class="${t2}"><a href="#3a" data-toggle="tab">Search by filename</a>
                </li>
			</ul>
            <div class="tab-content clearfix">
                <div class="tab-pane active" id="2a">
                    <div>
                        <%@include file="fileUpload.jsp" %>
                    </div>
                </div>
                <div class="tab-pane" id="3a">
                    <div>
                        <%@include file="searchDetails.jsp" %>
                    </div>
                </div>
            	<div class="has-error" style="color:#ffffff;padding:10px">
                    <c:if test="${not empty log}">
                        <c:forEach items="${log}" var="l">
                            <br class="help-inline">${l}</br>
                        </c:forEach>
                    </c:if>
                </div>
            </div>
            <div style="position: relative; bottom: 0; right: 0; width: 200px; text-align:left;">
                <a href="mailto:mailbox.salah@gmail.com" target="_top">Developed by Salahuddin.</a>
            </div>
        </div>

        <hr>

        <!-- Bootstrap core JavaScript
    ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="static/js/jquery.min.js"></script>
        <script src="static/js/bootstrap.min.js"></script>

    </body>
    </html>