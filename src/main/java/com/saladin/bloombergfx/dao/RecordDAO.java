/**
 * 
 */
package com.saladin.bloombergfx.dao;

import java.util.List;

import org.apache.commons.csv.CSVRecord;

/**
 * @author Salahuddin
 *
 */
public interface RecordDAO {

	void saveValid(List<CSVRecord> vList, Long fileID);

	void saveInvalid(List<CSVRecord> vList, Long fileID);
}
