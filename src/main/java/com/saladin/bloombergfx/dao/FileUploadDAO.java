/**
 * 
 */
package com.saladin.bloombergfx.dao;

import java.sql.SQLException;

/**
 * @author Salahuddin
 *
 */
public interface FileUploadDAO {

	Long addFile(String name);

	Long getFileByName(String fileName) throws SQLException;

	Long getCountValid(Long fid) throws SQLException;

	Long getCountInvalid(Long fid) throws SQLException;

}
