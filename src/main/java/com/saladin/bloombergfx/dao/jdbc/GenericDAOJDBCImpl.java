/**
 * 
 */
package com.saladin.bloombergfx.dao.jdbc;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;

import com.saladin.bloombergfx.dao.GenericDAO;

/**
 * @author Salahuddin
 *
 */

@PropertySource("classpath:query.properties")
public class GenericDAOJDBCImpl implements GenericDAO {
	@Value("${SQL_GET_FILEID_BY_NAME}")
	String SQL_GET_FILEID_BY_NAME;
	@Value("${SQL_INSERT_FILE_BY_NAME}")
	String SQL_INSERT_FILE_BY_NAME;
	@Value("${SQL_INSERT_VALIDRECORD_BY_FILEID}")
	String SQL_INSERT_VALIDRECORD_BY_FILEID;
	@Value("${SQL_INSERT_INVALIDRECORD_BY_FILEID}")
	String SQL_INSERT_INVALIDRECORD_BY_FILEID;
	@Value("${SQL_GET_COUNT_BY_CRY_CD}")
	String SQL_GET_COUNT_BY_CRY_CD;
	@Value("${SQL_UPDATE_COUNT_BY_CRCY_CD}")
	String SQL_UPDATE_COUNT_BY_CRCY_CD;
	@Value("${SQL_INSERT_DEAL}")
	String SQL_INSERT_DEAL;
	@Value("${SQL_GET_VALID_COUNT_BY_FILEID}")
	String SQL_GET_VALID_COUNT_BY_FILEID;
	@Value("${SQL_GET_INVALID_COUNT_BY_FILEID}")
	String SQL_GET_INVALID_COUNT_BY_FILEID;
	@Value("${SQL_GET_ALL_DEALS}")
	String SQL_GET_ALL_DEALS;

	ResultSetHandler<Long> longScalarHandler = new ScalarHandler<Long>();
	ResultSetHandler<Long> longresultHandler = new BeanHandler<Long>(Long.class) {
		@Override
		public Long handle(ResultSet rs) throws SQLException {
			if (rs.next()) {
				return rs.getLong(1);
			}
			return null;
		}
	};

	@Autowired
	DataSource dataSource;

	Connection getConection() throws SQLException {
		return dataSource.getConnection();
	}

	QueryRunner getQueryRunner() {
		return new QueryRunner(dataSource);
	}

	void close(Connection c) {
		if (c != null) {
			try {
				c.close();
			} catch (SQLException e) {
				// ignore
			}
		}
	}
}
