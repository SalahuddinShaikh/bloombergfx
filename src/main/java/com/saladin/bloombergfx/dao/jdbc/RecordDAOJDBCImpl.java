/**
 * 
 */
package com.saladin.bloombergfx.dao.jdbc;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.csv.CSVRecord;
import org.apache.commons.dbutils.QueryRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import com.saladin.bloombergfx.dao.RecordDAO;

/**
 * @author Salahuddin
 *
 */
@Repository
public class RecordDAOJDBCImpl extends GenericDAOJDBCImpl implements RecordDAO {

	private static final Logger logger = LoggerFactory.getLogger(RecordDAOJDBCImpl.class);

	public void batchInsert(String qry, List<CSVRecord> list, Long fileId) {

		long s = System.currentTimeMillis();
		Connection conection = null;
		try {
			conection = getConection();
			conection.setAutoCommit(false);
			/*
			 * StringBuilder sb = new StringBuilder(qry.substring(0,
			 * qry.indexOf("VALUES"))); sb.append("VALUES "); for (CSVRecord
			 * csvRecord : list) { sb.append("("); for (int i = 0; i < 6; i++) {
			 * sb.append(i==4 || i==5 ?"":"\""); sb.append(i != 5 ?
			 * csvRecord.get(i) : fileId); sb.append(i==4 || i==5 ?"":"\""); if
			 * (i != 5) sb.append(","); } sb.append("),\n"); } sb =
			 * sb.replace(sb.length() - 2, sb.length() - 1, ""); if
			 * (!list.isEmpty()) { String q = sb.toString();
			 * System.out.println(q); PreparedStatement stmt =
			 * conection.prepareStatement(q); stmt.execute(); }
			 */

			QueryRunner runner = new QueryRunner();
			if (runner != null)
				runner.batch(conection, qry, get2DArray(list, fileId)); //
			conection.commit();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(conection);
		}
		logger.info("Inserted " + list.size() + " records in " + (System.currentTimeMillis() - s) + " ms");
	}

	private Object[][] get2DArray(List<CSVRecord> arrayList, Long fileId) {
		List<Object[]> array = new ArrayList<>(arrayList.size());
		for (int i = 0; i < arrayList.size(); i++) {
			List<Object> rtn = new ArrayList<>(6);
			CSVRecord row = arrayList.get(i);
			if (row != null) {
				for (int j = 0; j < 5; j++) {
					rtn.add(row.size() > j ? row.get(j) : null);
				}
				rtn.add(5, String.valueOf(fileId));
			}
			array.add(rtn.toArray(new Object[0]));
		}
		return array.toArray(new Object[0][0]);
	}

	@Override
	public void saveValid(List<CSVRecord> vList, Long fileID) {
		batchInsert(SQL_INSERT_VALIDRECORD_BY_FILEID, vList, fileID);
	}

	@Override
	public void saveInvalid(List<CSVRecord> vList, Long fileID) {
		batchInsert(SQL_INSERT_INVALIDRECORD_BY_FILEID, vList, fileID);
	}
}
