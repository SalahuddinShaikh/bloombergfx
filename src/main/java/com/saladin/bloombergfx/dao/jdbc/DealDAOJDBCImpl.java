/**
 * 
 */
package com.saladin.bloombergfx.dao.jdbc;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.saladin.bloombergfx.dao.DealDAO;
import com.saladin.bloombergfx.model.DealCrncy;

/**
 * @author Salahuddin
 *
 */

@Repository
public class DealDAOJDBCImpl extends GenericDAOJDBCImpl implements DealDAO {
	private static final Logger logger = LoggerFactory.getLogger(DealDAOJDBCImpl.class);

	@Override
	public void updateDeals(Map<String, Long> sum) {
		try {
			Connection conection = getConection();
			for (Entry<String, Long> entry : sum.entrySet()) {
				QueryRunner runner = getQueryRunner();
				Long dealCount = runner.query(SQL_GET_COUNT_BY_CRY_CD, longresultHandler, entry.getKey());
				if (dealCount != null) {
					dealCount = dealCount + entry.getValue();
					runner.update(conection, SQL_UPDATE_COUNT_BY_CRCY_CD, dealCount, entry.getKey());
				} else {
					runner.insert(conection, SQL_INSERT_DEAL, longScalarHandler, entry.getKey(), entry.getValue());
				}
			}
			conection.commit();
		} catch (SQLException e) {
			e.printStackTrace();
			logger.error("failed to get deal count", e);
		}
	}

	@Override
	public List<DealCrncy> getAll() {
		try {
			return getQueryRunner().query(SQL_GET_ALL_DEALS, new BeanListHandler<DealCrncy>(DealCrncy.class));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

}
