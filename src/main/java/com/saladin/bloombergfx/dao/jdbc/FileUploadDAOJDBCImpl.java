/**
 * 
 */
package com.saladin.bloombergfx.dao.jdbc;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.saladin.bloombergfx.dao.FileUploadDAO;

/**
 * @author Salahuddin
 *
 */

@Repository
public class FileUploadDAOJDBCImpl extends GenericDAOJDBCImpl implements FileUploadDAO {
	private static final Logger logger = LoggerFactory.getLogger(FileUploadDAOJDBCImpl.class);


	@Override
	public Long addFile(String name) {
		Long id = null;
		Connection con = null;
		QueryRunner runner = getQueryRunner();
		try {
			con = getConection();
			id = getFileByName(name);
			if (id != null) {
				// file already present
				id = null;
			} else {
				// new upload
				id = runner.insert(con, SQL_INSERT_FILE_BY_NAME, longScalarHandler, name);
				con.commit();
			}
		} catch (SQLException e) {
			e.printStackTrace();
			logger.error("failed to get file id", e);
		}
		return id;
	}

	@Override
	public Long getFileByName(String fileName) throws SQLException {
		return getQueryRunner().query(SQL_GET_FILEID_BY_NAME, longresultHandler, fileName);
	}

	@Override
	public Long getCountValid(Long fid) throws SQLException {
		// TODO Auto-generated method stub
		return getQueryRunner().query(SQL_GET_VALID_COUNT_BY_FILEID, longresultHandler, fid);
	}

	@Override
	public Long getCountInvalid(Long fid) throws SQLException {
		return getQueryRunner().query(SQL_GET_INVALID_COUNT_BY_FILEID, longresultHandler, fid);
	}
}
