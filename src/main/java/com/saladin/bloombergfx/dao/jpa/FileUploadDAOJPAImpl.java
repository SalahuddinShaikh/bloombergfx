/**
 * 
 */
package com.saladin.bloombergfx.dao.jpa;

import java.sql.SQLException;
import java.util.List;

import javax.persistence.NoResultException;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;
import org.springframework.dao.DataAccessException;
import org.springframework.transaction.annotation.Transactional;

import com.saladin.bloombergfx.dao.FileUploadDAO;
import com.saladin.bloombergfx.model.FileUpld;
import com.saladin.bloombergfx.model.RecordValid;

/**
 * @author salah
 *
 */
public class FileUploadDAOJPAImpl extends GenericDAOJPAImpl implements FileUploadDAO {

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.saladin.bloombergfx.dao.FileUploadDAO#addFile(java.lang.String)
	 */
	@Override

	public Long addFile(String name) {

		StatelessSession session = getStatelessSession();
		Transaction t = session.beginTransaction();
		try {
			Long i = null;
			try {
				i = getFileByName(name);
			} catch (SQLException e) {
				// ignored
			}
			if (i != null) {
				return null;
			} else {
				FileUpld f = new FileUpld();
				f.setFileName(name);
				return (Long) session.insert(f);

			}
		} finally {
			commitTransaction(t);
			close(session);
		}
	}

	@Override
	public Long getFileByName(String fileName) throws SQLException {
		StatelessSession session = getStatelessSession();
		Transaction t = session.beginTransaction();
		try {
			Query query = session.getNamedQuery("FileUpld.findByNM");
			query.setParameter(0, fileName);
			List<FileUpld> list = (List<FileUpld>) query.list();
			if (list != null && list.size() > 0 && null != list.get(0))
				return list.get(0).getFileId();
		} finally {
			commitTransaction(t);
			close(session);
		}
		return null;
	}

	@Override
	public Long getCountValid(Long fid) throws SQLException {
		// TODO Auto-generated method stub
		Session session = getSession();
		try {
			return ((Long) session.createQuery("select count(p) from RecordValid p where p.fileUpld.fileId=?")
					.setParameter(0, fid).uniqueResult());
		} finally {
			close(session);
		}
	}

	@Override
	public Long getCountInvalid(Long fid) throws SQLException {
		Session session = getSession();
		try {
			return ((Long) session.createQuery("select count(p) from RecordInvalid p where p.fileUpld.fileId=?")
					.setParameter(0, fid).uniqueResult());
		} finally {
			close(session);
		}
	}
}
