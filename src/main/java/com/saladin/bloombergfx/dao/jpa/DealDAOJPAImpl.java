/**
 * 
 */
package com.saladin.bloombergfx.dao.jpa;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;

import com.saladin.bloombergfx.dao.DealDAO;
import com.saladin.bloombergfx.model.DealCrncy;

/**
 * @author salah
 *
 */
public class DealDAOJPAImpl extends GenericDAOJPAImpl implements DealDAO {

	private static final Logger logger = LoggerFactory.getLogger(DealDAOJPAImpl.class);

	@Override
	public void updateDeals(Map<String, Long> sum) {
		Map<String, Long> map = new HashMap<>();
		StatelessSession session = getStatelessSession();
		Transaction t = session.beginTransaction();
		try {
			for (Entry<String, Long> entry : sum.entrySet()) {
				String key = entry.getKey();
				Long value = entry.getValue();
				if (!map.containsKey(entry.getKey())) {
					// get from db
					DealCrncy deal = null;
					try {
						Query query = session.getNamedQuery("Deal.findByCD");
						query.setParameter(0, entry.getKey());
						List<DealCrncy> list = (List<DealCrncy>) query.list();
						if (list != null && list.size() > 0)
							deal = (DealCrncy) list.get(0);
					} catch (DataAccessException e) {
						e.printStackTrace();
					}
					if (deal != null) {
						value = value + deal.getDealCount();
					}
				}
				map.put(key, (map.get(key) != null ? map.get(key) : 0) + value);
				logger.info("updated deal");
			}
			//
			if (!map.isEmpty()) {
				try {
					for (Entry<String, Long> i : map.entrySet()) {
						DealCrncy o = (DealCrncy) session.get(DealCrncy.class, i.getKey());
						if (o != null) {
							o.setDealCount(i.getValue());
							session.update(o);
						} else {
							o = new DealCrncy();
							o.setCrncyCd(i.getKey());
							o.setDealCount(o.getDealCount());
							session.insert(o);
						}
					}
				} catch (HibernateException e) {
					e.printStackTrace();
				}
			}
		} finally {
			commitTransaction(t);
			close(session);
		}
	}

	@Override
	public List<DealCrncy> getAll() {
		Session session = getSession();
		try {
			return session.createCriteria(DealCrncy.class).list();
		} finally {
			close(session);
		}
	}
}
