/**
 * 
 */
package com.saladin.bloombergfx.dao.jpa;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.csv.CSVRecord;
import org.apache.commons.dbutils.QueryRunner;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.hibernate.jdbc.Work;

import com.saladin.bloombergfx.dao.RecordDAO;
import com.saladin.bloombergfx.model.FileUpld;
import com.saladin.bloombergfx.model.RecordInvalid;
import com.saladin.bloombergfx.model.RecordValid;

/**
 * @author salah
 *
 */
public class RecordDAOJPAImpl extends GenericDAOJPAImpl implements RecordDAO {

	@Override
	public void saveValid(final List<CSVRecord> vList, final Long fileID) {
		Session session = getSession();
		Transaction t = session.beginTransaction();
		try {
			final String sql = "INSERT INTO `bloombergfx`.`record_valid` (`DEAL_UID`, `CURENCY_CD_FRM`, `CURENCY_CD_TO`,`DEAL_TIMESTAMP`,`DEAL_AMNT`,`fileUpld_FILE_ID`) VALUES ( ?, ?, ?, ?, ?,?)";
			batchInsert(sql, session, vList);
		} catch (HibernateException e) {
			e.printStackTrace();
		} finally {
			commitTransaction(t);
			close(session);
		}
	}

	private void batchInsert(final String sql, final Session session, final List<CSVRecord> vList) {
		session.doWork(new Work() {
			@Override
			public void execute(Connection con) throws SQLException {
				Object[][] batchParam = getBatchParam();
				new QueryRunner().batch(con, sql, batchParam);
			}

			private Object[][] getBatchParam() {
				Object[][] batchParams = new Object[vList.size()][];
				for (int i = 0; i < vList.size(); i++) {
					CSVRecord r = vList.get(i);
					Object[] params = getParams(r);
					batchParams[i] = params;
				}
				return batchParams;
			}

			private Object[] getParams(CSVRecord r) {
				Object[] param = new Object[6];
				for (int i = 0; i < r.size(); i++) {
					param[i] = r.get(i);
				}
				for (int i = r.size(); i < 6; i++) {
					param[i] = null;
				}
				return param;
			}
		});
	}

	@Override
	public void saveInvalid(List<CSVRecord> iList, Long fileID) {
		Session session = getSession();
		Transaction t = session.beginTransaction();
		try {
			final String sql = "INSERT INTO `bloombergfx`.`record_invalid` (`DEAL_UID`, `CURENCY_CD_FRM`, `CURENCY_CD_TO`,`DEAL_TIMESTAMP`,`DEAL_AMNT`,`fileUpld_FILE_ID`) VALUES ( ?, ?, ?, ?, ?,?)";
			batchInsert(sql, session, iList);
		} catch (HibernateException e) {
			e.printStackTrace();
		} finally {
			commitTransaction(t);
			close(session);
		}
	}

	private RecordInvalid convertToInvalidRecord(CSVRecord a, FileUpld fileUpld) {
		if (null != a && a.size() >= 5) {
			RecordInvalid v = new RecordInvalid();
			v.setDealUid(a.get(0));
			v.setFrmCrncyCd(a.get(1));
			v.setToCrncyCd(a.get(2));
			v.setDealTimestmp(a.get(3));
			v.setDealAmnt(a.get(4));
			v.setFileUpld(fileUpld);
			return v;
		}
		return null;
	}

	private RecordValid convertToValidRecord(CSVRecord a, FileUpld fileUpld) {
		if (null != a && a.size() == 5) {
			RecordValid v = new RecordValid();
			v.setDealUid(a.get(0));
			v.setFrmCrncyCd(a.get(1));
			v.setToCrncyCd(a.get(2));
			v.setDealTimestmp(a.get(3));
			v.setDealAmnt(a.get(4));
			v.setFileUpld(fileUpld);
			return v;
		}
		return null;
	}

}
