/**
 * 
 */
package com.saladin.bloombergfx.dao.jpa;

import org.hibernate.CacheMode;
import org.hibernate.FlushMode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author salah
 *
 */
public class GenericDAOJPAImpl {

	@Autowired
	private SessionFactory sessionFactory;

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public StatelessSession getStatelessSession() {
		StatelessSession session = getSessionFactory().openStatelessSession();
		return session;
	}

	public void close(StatelessSession session) {
		if (session != null) {
			session.close();
		}
	}

	public Session getSession() {
		Session session = getSessionFactory().openSession();
		session.setFlushMode(FlushMode.MANUAL);
		session.setCacheMode(CacheMode.IGNORE);
		return session;
	}

	public void close(Session session) {
		if (session != null) {
			session.close();
		}
	}

	public void commitTransaction(Transaction t) {
		if (t != null) {
			t.commit();
		}
	}

}
