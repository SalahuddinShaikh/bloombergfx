/**
 * 
 */
package com.saladin.bloombergfx.dao;

import java.util.List;
import java.util.Map;

import com.saladin.bloombergfx.model.DealCrncy;

/**
 * @author Salahuddin
 *
 */
public interface DealDAO {

	void updateDeals(Map<String, Long> sum);

	List<DealCrncy> getAll();

}
