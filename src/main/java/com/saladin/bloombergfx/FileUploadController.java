/**
 * 
 */
package com.saladin.bloombergfx;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import javax.annotation.PreDestroy;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.saladin.bloombergfx.dao.FileUploadDAO;
import com.saladin.bloombergfx.model.DealCrncy;
import com.saladin.bloombergfx.service.IngestionService;

/**
 * @author Salahuddin
 *
 */
@Controller
public class FileUploadController {

	private static final Logger logger = Logger.getLogger(FileUploadController.class);

	@Autowired

	private IngestionService service;

	@RequestMapping(value = "/deals", method = RequestMethod.GET)
	public String getDeals(Model model) throws IOException, InterruptedException, ExecutionException {
		logger.info("Load Deals");
		List<DealCrncy> deals = null;
		List<String> log = new ArrayList<>();
		long start = System.currentTimeMillis();
		deals = service.getAllDeals();
		if (null != deals && deals.size() > 0) {
			logger.info("File details in " + (System.currentTimeMillis() - start) + " ms");
			log.add("Time Requied : " + (System.currentTimeMillis() - start) + " ms");
		} else {
			logger.info("No records found");
			log.add("No records found, Please try importing a file");
		}
		model.addAttribute("deals", deals);
		model.addAttribute("log", log);
		return "index";
	}

	@RequestMapping(value = "/details", method = RequestMethod.GET)
	public String getDetails(@RequestParam("fileName") String fileName, Model model)
			throws IOException, InterruptedException, ExecutionException {
		logger.info("File Uploaded");
		List<String> log = new ArrayList<>();
		if (!StringUtils.isBlank(fileName)) {
			long start = System.currentTimeMillis();
			Long fileId = service.getFile(fileName);
			if (null != fileId) {
				logger.info("File details in " + (System.currentTimeMillis() - start) + " ms");
				long v = service.getValidRecordCount(fileId);
				long i = service.getInvalidRecordCount(fileId);
				log.add("Filename : " + fileName);
				log.add("Valid Records : " + v);
				log.add("Invalid Records : " + i);
				log.add("Time Requied : " + (System.currentTimeMillis() - start) + " ms");
			} else {
				logger.info("File not found");
				log.add("File '" + fileName + "' not found in records");
			}
		} else {
			logger.info("not a valid file");
			log.add("Invalid file format");
		}
		model.addAttribute("log", log);
		return "index";
	}

	@RequestMapping(value = "/upload", method = RequestMethod.POST)
	public String uploadFile(@RequestParam("file") MultipartFile file, Model model)
			throws IOException, InterruptedException, ExecutionException {
		logger.info("File Uploaded");
		List<String> log = new ArrayList<>();
		if (FilenameUtils.getExtension(file.getOriginalFilename()).equalsIgnoreCase("CSV")) {
			long start = System.currentTimeMillis();
			Long fileId = service.insertFile(file.getOriginalFilename());
			if (null != fileId) {
				service.initIngestion(fileId, file.getInputStream());
				logger.info("File imported in " + (System.currentTimeMillis() - start) + " ms");
				log.add("File imported in " + (System.currentTimeMillis() - start) + " ms");
			} else {
				logger.info("File already Imported");
				log.add("File '" + file.getOriginalFilename() + "' alreay imported");
			}
		} else {
			logger.info("not a CSV file");
			log.add("Invalid file format");
		}

		model.addAttribute("log", log);
		return "index";
	}

	@PreDestroy
	private void destroy() {
		if (service != null)
			service.shutdown();
	}
}
