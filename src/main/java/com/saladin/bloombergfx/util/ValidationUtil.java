package com.saladin.bloombergfx.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.print.attribute.standard.MediaSize.ISO;

import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.propertyeditors.PatternEditor;

public class ValidationUtil {
	// ISO
	public static String RX_ISO_CRY = "(AED|AFN|ALL|AMD|ANG|AOA|ARS|AUD|AWG|AZN|BAM|BBD|BDT|BGN|BHD|BIF|BMD|BND|BOB|BOV|BRL|BSD|BTN|BWP|BYR|BZD|CAD|CDF|CHE|CHF|CHW|CLF|CLP|CNY|COP|COU|CRC|CUC|CUP|CVE|CZK|DJF|DKK|DOP|DZD|EGP|ERN|ETB|EUR|FJD|FKP|GBP|GEL|GHS|GIP|GMD|GNF|GTQ|GYD|HKD|HNL|HRK|HTG|HUF|IDR|ILS|INR|IQD|IRR|ISK|JMD|JOD|JPY|KES|KGS|KHR|KMF|KPW|KRW|KWD|KYD|KZT|LAK|LBP|LKR|LRD|LSL|LTL|LVL|LYD|MAD|MDL|MGA|MKD|MMK|MNT|MOP|MRO|MUR|MVR|MWK|MXN|MXV|MYR|MZN|NAD|NGN|NIO|NOK|NPR|NZD|OMR|PAB|PEN|PGK|PHP|PKR|PLN|PYG|QAR|RON|RSD|RUB|RWF|SAR|SBD|SCR|SDG|SEK|SGD|SHP|SLL|SOS|SRD|SSP|STD|SVC|SYP|SZL|THB|TJS|TMT|TND|TOP|TRY|TTD|TWD|TZS|UAH|UGX|USD|USN|USS|UYI|UYU|UZS|VEF|VND|VUV|WST|XAF|XAG|XAU|XBA|XBB|XBC|XBD|XCD|XDR|XFU|XOF|XPD|XPF|XPT|XSU|XTS|XUA|XXX|YER|ZAR|ZMW|ZWL)";
	// YYYY-MM-DD HH:MM:SS
	public static String RX_TIME = "(((19|20)\\d\\d)-(0?[1-9]|1[012])-(0?[1-9]|[12][0-9]|3[01]) ([2][0-3]|[0-1][0-9]|[1-9]):[0-5][0-9]:([0-5][0-9]|[6][0]))";
	// Amount
	public static String RX_AMT = "(\\d+\\.\\d{1,2})";// "[1-9][\\.\\d]*(,\\d+)?";//"([0-9]+([.][0-9]{1,2})?)";

	public static Pattern ISO_CRY, TIME, AMT;

	//
	static {
		ISO_CRY = Pattern.compile(RX_ISO_CRY);
		AMT = Pattern.compile(RX_AMT);
		TIME = Pattern.compile(RX_TIME);
	}

	public static boolean isValid(CSVRecord a) {
		return a != null && a.size() == 5 && StringUtils.isNotBlank(a.get(0)) && ISO_CRY.matcher(a.get(1)).matches()
				&& ISO_CRY.matcher(a.get(2)).matches() && TIME.matcher(a.get(3)).matches()
				&& AMT.matcher(a.get(4)).matches();
	}
}
