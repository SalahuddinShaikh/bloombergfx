package com.saladin.bloombergfx.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.regex.Pattern;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

import com.mifmif.common.regex.Generex;

public class CSVGenerater {

	private static final Logger logger = Logger.getLogger(CSVGenerater.class);

	private static void verify(String filename) throws FileNotFoundException, IOException {
		long v = 0, i = 0;
		Iterable<CSVRecord> records = CSVFormat.EXCEL
				.parse(new InputStreamReader(new FileInputStream(filename), Charset.forName("UTF-8")));
		for (CSVRecord csvRecord : records) {
			if (ValidationUtil.isValid(csvRecord))
				v++;
			else
				i++;
		}
		System.out.println("total:" + (v + i) + "\nvalid:" + v + "\ninvalid:" + i);
	}

	public static void main(String[] args) throws FileNotFoundException, IOException {
		System.out.println("start");
		int validOutOf100 = 70;
		String fileName = "BigCSV_" + validOutOf100 + "%_Valid.csv";
		generate(fileName, validOutOf100);
		verify(fileName);
		System.out.println("end");
	}

	private static void generate(String filename, int validOutOf10) {
		logger.info("generating csv");
		int total = 100 * 1000;
		int div = 100 / validOutOf10;
		Generex country = new Generex(ValidationUtil.RX_ISO_CRY);
		Generex amount = new Generex(ValidationUtil.RX_AMT);
		Generex timestamp = new Generex(ValidationUtil.RX_TIME);
		//
		FileOutputStream fos = null;
		try {
			fos = new FileOutputStream(filename);
			for (int i = 0; i < total; i++) {
				String line = String.format("\n%d,%s,%s,%s,%s", i, country.random() + (i % (div) == 0 ? "#" : ""),
						country.random(), timestamp.random(), amount.random());
				IOUtils.write(line, fos);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			IOUtils.closeQuietly(fos);
		}
		logger.info("end");
	}

}
