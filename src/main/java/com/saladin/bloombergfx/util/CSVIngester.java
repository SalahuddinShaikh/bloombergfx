package com.saladin.bloombergfx.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

import org.apache.commons.csv.CSVRecord;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Service;

import com.saladin.bloombergfx.dao.RecordDAO;

@Service
@Configurable
public class CSVIngester {

	private static final Logger logger = Logger.getLogger(CSVIngester.class);

	@Autowired
	private ExecutorService pool;
	@Autowired
	private RecordDAO recordDAO;

	public Future<Map<String, Long>> invoke(final Long f, final List<CSVRecord> records) {
		logger.info("processing records : " + records.size());
		return pool.submit(new Callable<Map<String, Long>>() {

			@Override
			public Map<String, Long> call() throws Exception {
				Map<String, Long> map = new HashMap<>();
				List<CSVRecord> vList = new ArrayList<>();
				List<CSVRecord> iList = new ArrayList<>();
				for (CSVRecord csvRecord : records) {
					if (ValidationUtil.isValid(csvRecord)) {
						// prepare accumulative count
						if (map.containsKey(csvRecord.get(1))) {
							map.put(csvRecord.get(1), map.get(csvRecord.get(1)) + 1);
						} else {
							map.put(csvRecord.get(1), 1l);
						}
						// add to valid table
						// vList.add(convertCSVtoValidRec(csvRecord, f));
						vList.add(csvRecord);
					} else {
						// add to invalid table
						iList.add(csvRecord);
					} // else
				} // for
				if (!vList.isEmpty())
					recordDAO.saveValid(vList, f);
				if (!iList.isEmpty())
					recordDAO.saveInvalid(iList, f);
				return map;
			}// call
		});
	}

	public void shutdown() {
		logger.info("shutting down pool");
		pool.shutdown();
	}	
	/*
	 * public static void main(String a[]) { System.out.println("start");
	 * CSVIngester engine = new CSVIngester(); List<Future<Map<String, Long>>>
	 * future = new ArrayList<>(); for (int i = 0; i < 10; i++) {
	 * future.add(engine.invoke(Arrays.asList(new String[] { "", "", "" }))); }
	 * System.out.println("fture"); for (Future<Map<String, Long>> f : future) {
	 * try { System.out.println("Output:" + f.get().toString()); } catch
	 * (InterruptedException | ExecutionException e) { // TODO Auto-generated
	 * catch block e.printStackTrace(); } } System.out.println("shutdown");
	 * engine.shutdown(); System.out.println("end"); }
	 */

	public void setPool(ExecutorService pool) {
		this.pool = pool;
	}

	public void setRecordDAO(RecordDAO recordDAO) {
		this.recordDAO = recordDAO;
	}

}
