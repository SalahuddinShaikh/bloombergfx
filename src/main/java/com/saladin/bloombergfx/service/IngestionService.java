package com.saladin.bloombergfx.service;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.concurrent.ExecutionException;

import com.saladin.bloombergfx.model.DealCrncy;

/**
 * @author Salahuddin
 *
 */
public interface IngestionService {

	void initIngestion(Long fileId, InputStream in) throws IOException, InterruptedException, ExecutionException;

	Long insertFile(String name);

	void shutdown();

	Long getFile(String fileName);

	long getValidRecordCount(Long fileId);

	long getInvalidRecordCount(Long fileId);

	List<DealCrncy> getAllDeals();
}
