/**
 * 
 */
package com.saladin.bloombergfx.service;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.saladin.bloombergfx.dao.DealDAO;
import com.saladin.bloombergfx.dao.FileUploadDAO;
import com.saladin.bloombergfx.model.DealCrncy;
import com.saladin.bloombergfx.util.CSVIngester;

/**
 * @author Salahuddin
 *
 */
@Service
public class IngestionServiceImpl implements IngestionService {

	private static final Logger logger = Logger.getLogger(IngestionServiceImpl.class);

	@Autowired
	private DealDAO dealDAO;
	@Autowired
	private FileUploadDAO fileUploadDAO;
	@Value("${app.max_thread}")
	private Integer threadSize;
	@Autowired
	private CSVIngester ingester;
	@Value("${app.batch_size}")
	private Integer batch;

	@Override
	public void initIngestion(Long file, InputStream in) throws IOException, InterruptedException, ExecutionException {
		logger.info("starting ingestion");
		long s = System.currentTimeMillis();
		Iterable<CSVRecord> records = CSVFormat.EXCEL.parse(new InputStreamReader(in, Charset.forName("UTF-8")));
		List<CSVRecord> list = new ArrayList<>(batch);
		List<Future<Map<String, Long>>> ftrs = new ArrayList<>();
		int index = 0;
		for (CSVRecord csvRecord : records) {
			index++;
			list.add(csvRecord);
			synchronized (ingester) {
				if (index % batch == 0 && !list.isEmpty()) {
					ftrs.add(ingester.invoke(file, list));
					list = new ArrayList<>(batch);
				}
			}
		}
		ftrs.add(ingester.invoke(file, list));
		logger.info("Consolidating deals");
		Map<String, Long> sum = new HashMap<>();
		for (Future<Map<String, Long>> future : ftrs) {
			Map<String, Long> map = future.get();
			for (Entry<String, Long> entry : map.entrySet()) {
				if (sum.containsKey(entry.getKey())) {
					sum.put(entry.getKey(), sum.get(entry.getKey()) + entry.getValue());
				} else {
					sum.put(entry.getKey(), entry.getValue());
				}
			}
		}
		dealDAO.updateDeals(sum);
		logger.info(String.format("Invokation completed in %s ms", (System.currentTimeMillis() - s)));
	}

	@Override
	public Long insertFile(String name) {
		return fileUploadDAO.addFile(name);
	}

	public void setDealDAO(DealDAO dealDAO) {
		this.dealDAO = dealDAO;
	}

	public void setFileUploadDAO(FileUploadDAO fileUploadDAO) {
		this.fileUploadDAO = fileUploadDAO;
	}

	public void setThreadSize(Integer threadSize) {
		this.threadSize = threadSize;
	}

	public void setIngester(CSVIngester ingester) {
		this.ingester = ingester;
	}

	public void setBatch(Integer batch) {
		this.batch = batch;
	}

	public void shutdown() {
		ingester.shutdown();
	}

	@Override
	public Long getFile(String fileName) {
		try {
			return fileUploadDAO.getFileByName(fileName);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public long getValidRecordCount(Long fid) {
		// TODO Auto-generated method stub
		try {
			return fileUploadDAO.getCountValid(fid);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public long getInvalidRecordCount(Long fid) {
		// TODO Auto-generated method stub
		try {
			return fileUploadDAO.getCountInvalid(fid);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public List<DealCrncy> getAllDeals() {
		// TODO Auto-generated method stub
		return dealDAO.getAll();
	}

}
